defmodule SeasonalCalendar.MonthsTest do
  use SeasonalCalendar.DataCase

  alias SeasonalCalendar.Months

  describe "months" do
    alias SeasonalCalendar.Months.Month

    @valid_attrs %{name: "some name", slug: "some slug"}
    @update_attrs %{name: "some updated name", slug: "some updated slug"}
    @invalid_attrs %{name: nil, slug: nil}

    def month_fixture(attrs \\ %{}) do
      {:ok, month} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Months.create_month()

      month
    end

    test "list_months/0 returns all months" do
      month = month_fixture()
      assert Months.list_months() == [month]
    end

    test "get_month!/1 returns the month with given id" do
      month = month_fixture()
      assert Months.get_month!(month.id) == month
    end

    test "create_month/1 with valid data creates a month" do
      assert {:ok, %Month{} = month} = Months.create_month(@valid_attrs)
      assert month.name == "some name"
      assert month.slug == "some slug"
    end

    test "create_month/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Months.create_month(@invalid_attrs)
    end

    test "update_month/2 with valid data updates the month" do
      month = month_fixture()
      assert {:ok, %Month{} = month} = Months.update_month(month, @update_attrs)
      assert month.name == "some updated name"
      assert month.slug == "some updated slug"
    end

    test "update_month/2 with invalid data returns error changeset" do
      month = month_fixture()
      assert {:error, %Ecto.Changeset{}} = Months.update_month(month, @invalid_attrs)
      assert month == Months.get_month!(month.id)
    end

    test "delete_month/1 deletes the month" do
      month = month_fixture()
      assert {:ok, %Month{}} = Months.delete_month(month)
      assert_raise Ecto.NoResultsError, fn -> Months.get_month!(month.id) end
    end

    test "change_month/1 returns a month changeset" do
      month = month_fixture()
      assert %Ecto.Changeset{} = Months.change_month(month)
    end
  end
end
