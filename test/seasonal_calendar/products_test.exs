defmodule SeasonalCalendar.ProductsTest do
  use SeasonalCalendar.DataCase

  alias SeasonalCalendar.Products

  describe "products" do
    alias SeasonalCalendar.Products.Product

    @valid_attrs %{name: "some name", slug: "some slug"}
    @update_attrs %{name: "some updated name", slug: "some updated slug"}
    @invalid_attrs %{name: nil, slug: nil}

    def product_fixture(attrs \\ %{}) do
      {:ok, product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Products.create_product()

      product
    end

    test "list_products/0 returns all products" do
      product = product_fixture()
      assert Products.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = product_fixture()
      assert Products.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      assert {:ok, %Product{} = product} = Products.create_product(@valid_attrs)
      assert product.name == "some name"
      assert product.slug == "some slug"
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Products.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = product_fixture()
      assert {:ok, %Product{} = product} = Products.update_product(product, @update_attrs)
      assert product.name == "some updated name"
      assert product.slug == "some updated slug"
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = product_fixture()
      assert {:error, %Ecto.Changeset{}} = Products.update_product(product, @invalid_attrs)
      assert product == Products.get_product!(product.id)
    end

    test "delete_product/1 deletes the product" do
      product = product_fixture()
      assert {:ok, %Product{}} = Products.delete_product(product)
      assert_raise Ecto.NoResultsError, fn -> Products.get_product!(product.id) end
    end

    test "change_product/1 returns a product changeset" do
      product = product_fixture()
      assert %Ecto.Changeset{} = Products.change_product(product)
    end
  end

  describe "regionalproducts" do
    alias SeasonalCalendar.Products.RegionalProduct

    @valid_attrs %{availabilities: %{}, name: "some name", slug: "some slug"}
    @update_attrs %{availabilities: %{}, name: "some updated name", slug: "some updated slug"}
    @invalid_attrs %{availabilities: nil, name: nil, slug: nil}

    def regional_product_fixture(attrs \\ %{}) do
      {:ok, regional_product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Products.create_regional_product()

      regional_product
    end

    test "list_regionalproducts/0 returns all regionalproducts" do
      regional_product = regional_product_fixture()
      assert Products.list_regionalproducts() == [regional_product]
    end

    test "get_regional_product!/1 returns the regional_product with given id" do
      regional_product = regional_product_fixture()
      assert Products.get_regional_product!(regional_product.id) == regional_product
    end

    test "create_regional_product/1 with valid data creates a regional_product" do
      assert {:ok, %RegionalProduct{} = regional_product} = Products.create_regional_product(@valid_attrs)
      assert regional_product.availabilities == %{}
      assert regional_product.name == "some name"
      assert regional_product.slug == "some slug"
    end

    test "create_regional_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Products.create_regional_product(@invalid_attrs)
    end

    test "update_regional_product/2 with valid data updates the regional_product" do
      regional_product = regional_product_fixture()
      assert {:ok, %RegionalProduct{} = regional_product} = Products.update_regional_product(regional_product, @update_attrs)
      assert regional_product.availabilities == %{}
      assert regional_product.name == "some updated name"
      assert regional_product.slug == "some updated slug"
    end

    test "update_regional_product/2 with invalid data returns error changeset" do
      regional_product = regional_product_fixture()
      assert {:error, %Ecto.Changeset{}} = Products.update_regional_product(regional_product, @invalid_attrs)
      assert regional_product == Products.get_regional_product!(regional_product.id)
    end

    test "delete_regional_product/1 deletes the regional_product" do
      regional_product = regional_product_fixture()
      assert {:ok, %RegionalProduct{}} = Products.delete_regional_product(regional_product)
      assert_raise Ecto.NoResultsError, fn -> Products.get_regional_product!(regional_product.id) end
    end

    test "change_regional_product/1 returns a regional_product changeset" do
      regional_product = regional_product_fixture()
      assert %Ecto.Changeset{} = Products.change_regional_product(regional_product)
    end
  end

  describe "monthdata" do
    alias SeasonalCalendar.Products.MonthData

    @valid_attrs %{april: 42, august: 42, december: 42, february: 42, january: 42, july: 42, june: 42, march: 42, may: 42, november: 42, october: 42, september: 42}
    @update_attrs %{april: 43, august: 43, december: 43, february: 43, january: 43, july: 43, june: 43, march: 43, may: 43, november: 43, october: 43, september: 43}
    @invalid_attrs %{april: nil, august: nil, december: nil, february: nil, january: nil, july: nil, june: nil, march: nil, may: nil, november: nil, october: nil, september: nil}

    def month_data_fixture(attrs \\ %{}) do
      {:ok, month_data} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Products.create_month_data()

      month_data
    end

    test "list_monthdata/0 returns all monthdata" do
      month_data = month_data_fixture()
      assert Products.list_monthdata() == [month_data]
    end

    test "get_month_data!/1 returns the month_data with given id" do
      month_data = month_data_fixture()
      assert Products.get_month_data!(month_data.id) == month_data
    end

    test "create_month_data/1 with valid data creates a month_data" do
      assert {:ok, %MonthData{} = month_data} = Products.create_month_data(@valid_attrs)
      assert month_data.april == 42
      assert month_data.august == 42
      assert month_data.december == 42
      assert month_data.february == 42
      assert month_data.january == 42
      assert month_data.july == 42
      assert month_data.june == 42
      assert month_data.march == 42
      assert month_data.may == 42
      assert month_data.november == 42
      assert month_data.october == 42
      assert month_data.september == 42
    end

    test "create_month_data/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Products.create_month_data(@invalid_attrs)
    end

    test "update_month_data/2 with valid data updates the month_data" do
      month_data = month_data_fixture()
      assert {:ok, %MonthData{} = month_data} = Products.update_month_data(month_data, @update_attrs)
      assert month_data.april == 43
      assert month_data.august == 43
      assert month_data.december == 43
      assert month_data.february == 43
      assert month_data.january == 43
      assert month_data.july == 43
      assert month_data.june == 43
      assert month_data.march == 43
      assert month_data.may == 43
      assert month_data.november == 43
      assert month_data.october == 43
      assert month_data.september == 43
    end

    test "update_month_data/2 with invalid data returns error changeset" do
      month_data = month_data_fixture()
      assert {:error, %Ecto.Changeset{}} = Products.update_month_data(month_data, @invalid_attrs)
      assert month_data == Products.get_month_data!(month_data.id)
    end

    test "delete_month_data/1 deletes the month_data" do
      month_data = month_data_fixture()
      assert {:ok, %MonthData{}} = Products.delete_month_data(month_data)
      assert_raise Ecto.NoResultsError, fn -> Products.get_month_data!(month_data.id) end
    end

    test "change_month_data/1 returns a month_data changeset" do
      month_data = month_data_fixture()
      assert %Ecto.Changeset{} = Products.change_month_data(month_data)
    end
  end
end
