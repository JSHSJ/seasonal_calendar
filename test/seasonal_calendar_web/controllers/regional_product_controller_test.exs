defmodule SeasonalCalendarWeb.RegionalProductControllerTest do
  use SeasonalCalendarWeb.ConnCase

  alias SeasonalCalendar.Products

  @create_attrs %{availabilities: %{}, name: "some name", slug: "some slug"}
  @update_attrs %{availabilities: %{}, name: "some updated name", slug: "some updated slug"}
  @invalid_attrs %{availabilities: nil, name: nil, slug: nil}

  def fixture(:regional_product) do
    {:ok, regional_product} = Products.create_regional_product(@create_attrs)
    regional_product
  end

  describe "index" do
    test "lists all regionalproducts", %{conn: conn} do
      conn = get(conn, Routes.regional_product_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Regionalproducts"
    end
  end

  describe "new regional_product" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.regional_product_path(conn, :new))
      assert html_response(conn, 200) =~ "New Regional product"
    end
  end

  describe "create regional_product" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.regional_product_path(conn, :create), regional_product: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.regional_product_path(conn, :show, id)

      conn = get(conn, Routes.regional_product_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Regional product"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.regional_product_path(conn, :create), regional_product: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Regional product"
    end
  end

  describe "edit regional_product" do
    setup [:create_regional_product]

    test "renders form for editing chosen regional_product", %{conn: conn, regional_product: regional_product} do
      conn = get(conn, Routes.regional_product_path(conn, :edit, regional_product))
      assert html_response(conn, 200) =~ "Edit Regional product"
    end
  end

  describe "update regional_product" do
    setup [:create_regional_product]

    test "redirects when data is valid", %{conn: conn, regional_product: regional_product} do
      conn = put(conn, Routes.regional_product_path(conn, :update, regional_product), regional_product: @update_attrs)
      assert redirected_to(conn) == Routes.regional_product_path(conn, :show, regional_product)

      conn = get(conn, Routes.regional_product_path(conn, :show, regional_product))
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, regional_product: regional_product} do
      conn = put(conn, Routes.regional_product_path(conn, :update, regional_product), regional_product: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Regional product"
    end
  end

  describe "delete regional_product" do
    setup [:create_regional_product]

    test "deletes chosen regional_product", %{conn: conn, regional_product: regional_product} do
      conn = delete(conn, Routes.regional_product_path(conn, :delete, regional_product))
      assert redirected_to(conn) == Routes.regional_product_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.regional_product_path(conn, :show, regional_product))
      end
    end
  end

  defp create_regional_product(_) do
    regional_product = fixture(:regional_product)
    {:ok, regional_product: regional_product}
  end
end
