defmodule SeasonalCalendar.Repo.Migrations.CreateRegions do
  use Ecto.Migration

  def change do
    create table(:regions) do
      add :name, :string
      add :slug, :string

      timestamps()
    end

  end
end
