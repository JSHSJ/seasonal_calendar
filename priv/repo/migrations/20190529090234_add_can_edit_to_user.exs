defmodule SeasonalCalendar.Repo.Migrations.AddCanEditToUser do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :can_edit, references(:regions, on_delete: :nothing)
    end
  end
end
