defmodule SeasonalCalendar.Repo.Migrations.RemoveOldAuthentication do
  use Ecto.Migration

  def change do
    drop table(:credentials)
    drop table(:token)
  end
end
