defmodule SeasonalCalendar.Repo.Migrations.RenameRegionAndTypeToIdOnRp do
  use Ecto.Migration

  def change do
    alter table(:regionalproducts) do
      remove :type
      remove :region
      add :type_id, references(:products, on_delete: :delete_all), null: false
      add :region_id, references(:regions, on_delete: :delete_all), null: false
    end

#    create index(:regionalproducts, [:type_id])
#    create index(:regionalproducts, [:region_id])

  end
end
