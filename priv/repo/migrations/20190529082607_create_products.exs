defmodule SeasonalCalendar.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :name, :string
      add :slug, :string
      add :category, references(:categories, on_delete: :nothing)

      timestamps()
    end

    create index(:products, [:category])
  end
end
