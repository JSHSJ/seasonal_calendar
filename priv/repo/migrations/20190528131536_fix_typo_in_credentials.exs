defmodule :"Elixir.SeasonalCalendar.Repo.Migrations.Fix_typo_in_credentials" do
  use Ecto.Migration

  def change do
    alter table(:credentials) do
      remove :pasword_hash
      add :password_hash, :string, null: false
    end
  end
end
