defmodule SeasonalCalendar.Repo.Migrations.CreateRegionalproducts do
  use Ecto.Migration

  def change do
    create table(:regionalproducts) do
      add :name, :string
      add :slug, :string
      add :availabilities, :map
      add :type, references(:products, on_delete: :nothing)
      add :region, references(:regions, on_delete: :nothing)

      timestamps()
    end

    create index(:regionalproducts, [:type])
    create index(:regionalproducts, [:region])
  end
end
