defmodule SeasonalCalendar.Repo.Migrations.FixEditorShitV3 do
  use Ecto.Migration

  def change do
    alter table(:regions) do
      remove :editable_by_id
    end

    create table(:region_editors, primary_key: false) do
      add :region_id, references(:regions)
      add :user_id, references(:users)
    end
  end
end
