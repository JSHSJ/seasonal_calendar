defmodule SeasonalCalendar.Repo.Migrations.AllowDeleteOfRegionalProducts do
  use Ecto.Migration

  def change do
    alter table(:regionalproducts) do
      remove :type
      remove :region
      add :type, references(:products, on_delete: :delete_all)
      add :region, references(:regions, on_delete: :delete_all)
    end

  end
end
