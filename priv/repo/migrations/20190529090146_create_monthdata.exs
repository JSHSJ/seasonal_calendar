defmodule SeasonalCalendar.Repo.Migrations.CreateMonthdata do
  use Ecto.Migration

  def change do
    create table(:monthdata) do
      add :january, :integer
      add :february, :integer
      add :march, :integer
      add :april, :integer
      add :may, :integer
      add :june, :integer
      add :july, :integer
      add :august, :integer
      add :september, :integer
      add :october, :integer
      add :november, :integer
      add :december, :integer

      timestamps()
    end

  end
end
