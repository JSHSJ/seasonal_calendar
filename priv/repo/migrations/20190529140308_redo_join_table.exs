defmodule SeasonalCalendar.Repo.Migrations.RedoJoinTable do
  use Ecto.Migration

  def change do
    drop table(:region_editors)

    create table(:region_editors, primary_key: false) do
      add :region_id, references(:regions)
      add :user_id, references(:users)
    end

  end
end
