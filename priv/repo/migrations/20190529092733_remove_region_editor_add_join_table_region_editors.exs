defmodule SeasonalCalendar.Repo.Migrations.RemoveRegionEditorAddJoinTableRegionEditors do
  use Ecto.Migration

  def change do
    alter table(:users) do
      remove :can_edit, references(:regions, on_delete: :nothing)
    end

    create table(:region_editors) do
      add :region_id, references(:regions)
      add :user_id, references(:users)
    end

    create unique_index(:region_editors, [:region_id, :user_id])
  end
end
