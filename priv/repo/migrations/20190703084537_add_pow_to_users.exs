defmodule SeasonalCalendar.Repo.Migrations.AddPowToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :email, :string, null: false
      add :password_hash, :string
    end

    create unique_index(:users, [:email])
  end
end
