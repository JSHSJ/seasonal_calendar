defmodule SeasonalCalendar.Repo.Migrations.CreateMonths do
  use Ecto.Migration

  def change do
    create table(:months) do
      add :name, :string
      add :slug, :string

      timestamps()
    end

  end
end
