defmodule SeasonalCalendar.Repo.Migrations.FixEditorShit2 do
  use Ecto.Migration

  def change do
    alter table(:regions) do
#      remove :editable_by
      add :editable_by_id, references(:users)
    end

  end
end
