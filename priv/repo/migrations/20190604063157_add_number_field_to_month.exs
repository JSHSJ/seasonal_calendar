defmodule SeasonalCalendar.Repo.Migrations.AddNumberFieldToMonth do
  use Ecto.Migration

  def change do
    alter table(:months) do
      add :number, :integer
    end

  end
end
