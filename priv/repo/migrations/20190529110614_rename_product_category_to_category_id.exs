defmodule SeasonalCalendar.Repo.Migrations.RenameProductCategoryToCategoryId do
  use Ecto.Migration

  def change do
    alter table(:products) do
      remove :category, references(:categories, on_delete: :nothing)
      add :category_id, references(:categories, on_delete: :nothing)
    end

  end
end
