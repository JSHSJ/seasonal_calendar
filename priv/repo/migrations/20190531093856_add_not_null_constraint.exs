defmodule SeasonalCalendar.Repo.Migrations.AddNotNullConstraint do
  use Ecto.Migration

  def change do
    alter table(:products) do
      remove :category_id
      add :category_id, references(:categories, on_delete: :delete_all), null: false
    end
  end
end
