defmodule SeasonalCalendar.Repo.Migrations.CreateToken do
  use Ecto.Migration

  def change do
    create table(:token) do
      add :invite_string, :string
      add :is_admin, :boolean, default: false, null: false

      timestamps()
    end

  end
end
