# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     SeasonalCalendar.Repo.insert!(%SeasonalCalendar.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias SeasonalCalendar.Months

Months.create_month(%{name: "January", number: 1})
Months.create_month(%{name: "February", number: 2})
Months.create_month(%{name: "March", number: 3})
Months.create_month(%{name: "April", number: 4})
Months.create_month(%{name: "May", number: 5})
Months.create_month(%{name: "June", number: 6})
Months.create_month(%{name: "July", number: 7})
Months.create_month(%{name: "August", number: 8})
Months.create_month(%{name: "September", number: 9})
Months.create_month(%{name: "October", number: 10})
Months.create_month(%{name: "November", number: 11})
Months.create_month(%{name: "December", number: 12})
