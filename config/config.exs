# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :seasonal_calendar,
  ecto_repos: [SeasonalCalendar.Repo]

# Configures the endpoint
config :seasonal_calendar, SeasonalCalendarWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "gQeJ5x6kmGS35YZ4PDha3QGEbikoO+usxMEC/DOHjC4AtrUp+6EtqNkRG1Q1dEn/",
  render_errors: [view: SeasonalCalendarWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SeasonalCalendar.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [
    signing_salt: "SKuwohSyNGLuEEOW/2CmZu47JEA4mluPYaa7RPlsh02bWgJzfNcGhzkPG6NlHaA"
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :phoenix,
       template_engines: [leex: Phoenix.LiveView.Engine]


config :seasonal_calendar, :pow,
  user: SeasonalCalendar.Accounts.User,
  repo: SeasonalCalendar.Repo,
  web_module: SeasonalCalendarWeb,
  extensions: [PowResetPassword, PowInvitation],
  controller_callbacks: Pow.Extension.Phoenix.ControllerCallbacks

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
