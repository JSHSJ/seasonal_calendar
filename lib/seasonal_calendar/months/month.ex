defmodule SeasonalCalendar.Months.Month do
  use Ecto.Schema
  import Ecto.Changeset

  schema "months" do
    field :name, :string
    field :slug, :string
    field :number, :integer

    timestamps()
  end

  @doc false
  def changeset(month, attrs) do
    month
    |> cast(attrs, [:name, :number])
    |> validate_required([:name, :number])
    |> slugify_name()
  end

  defp slugify_name(changeset) do
    case fetch_change(changeset, :name) do
      {:ok, new_name} -> put_change(changeset, :slug, slugify(new_name))
      :error -> changeset
    end
  end

  defp slugify(str) do
    str
    |> String.downcase()
    |> String.replace(~r/[^\w-]+/u, "-")
    |> String.replace("ü", "ue")
    |> String.replace("ä", "ae")
    |> String.replace("ö", "oe")
  end
end

defimpl Phoenix.Param, for: SeasonalCalendar.Months.Month do
  def to_param(%{slug: slug}) do
    "#{slug}"
  end
end

