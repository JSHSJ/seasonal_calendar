defmodule SeasonalCalendar.Regions do
  @moduledoc """
  The Regions context.
  """

  import Ecto.Query, warn: false
  alias SeasonalCalendar.Repo

  alias SeasonalCalendar.Regions.Region
  alias SeasonalCalendar.Accounts.User

  @doc """
  Returns the list of regions.

  ## Examples

      iex> list_regions()
      [%Region{}, ...]

  """
  def list_regions do
    Region
    |> order_by(asc: :name)
    |> preload(:editors)
    |> Repo.all()
  end

  def get_editable_regions_for_user(userid) do
    #    Region
    #    |> join(:left, [r], u in assoc(r, :editors))
    #    |> preload(:editors)
    #    |> where([r], ^userid in r.editors)
    #    |> order_by(asc: :name)
    #    |> Repo.all()
    query =
      (from r in Region,
            join: u in assoc(r, :editors),
            where: u.id == ^userid,
            preload: [
              editors: u
            ])
    Repo.all(query)
  end

  @doc """
  Gets a single region.

  Raises `Ecto.NoResultsError` if the Region does not exist.

  ## Examples

      iex> get_region!(123)
      %Region{}

      iex> get_region!(456)
      ** (Ecto.NoResultsError)

  """
  def get_region!(slug) do
    query =
      from r in Region,
           where: r.slug == ^slug,
           preload: [:editors]
    Repo.one(query)
  end

  def get_region_by_id!(id) do
    Repo.get!(Region, id)
  end

  @doc """
  Creates a region.

  ## Examples

      iex> create_region(%{field: value})
      {:ok, %Region{}}

      iex> create_region(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_region(attrs \\ %{}) do
    %Region{}
    |> Repo.preload(:editors)
    |> Region.changeset(attrs)
    |> Repo.insert()
  end



  def update_region_editors(region, attrs) do
    editors = get_editors(region, attrs)

    region
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.put_assoc(:editors, editors)
    |> Repo.update()
  end

  defp get_editors(region, attrs) do
    case Map.has_key?(attrs, "editors") do
      true -> User
              |> where([user], user.id in ^attrs["editors"])
              |> Repo.all()
      false -> []
    end
  end


  @doc """
  Updates a region.

  ## Examples

      iex> update_region(region, %{field: new_value})
      {:ok, %Region{}}

      iex> update_region(region, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_region(%Region{} = region, attrs) do
    region
    |> Repo.preload(:editors)
    |> Region.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Region.

  ## Examples

      iex> delete_region(region)
      {:ok, %Region{}}

      iex> delete_region(region)
      {:error, %Ecto.Changeset{}}

  """
  def delete_region(%Region{} = region) do
    Repo.delete(region)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking region changes.

  ## Examples

      iex> change_region(region)
      %Ecto.Changeset{source: %Region{}}

  """
  def change_region(%Region{} = region) do
    Region.changeset(region, %{editors: []})
  end
end
