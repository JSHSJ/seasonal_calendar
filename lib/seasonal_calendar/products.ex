defmodule SeasonalCalendar.Products do
  @moduledoc """
  The Products context.
  """

  import Ecto.Query, warn: false
  alias SeasonalCalendar.Repo

  alias SeasonalCalendar.Products.Product
  alias SeasonalCalendar.Regions.Region
  alias SeasonalCalendar.Products.RegionalProduct


  @doc """
  Returns the list of products.

  ## Examples

      iex> list_products()
      [%Product{}, ...]

  """
  def list_products do
    Product
    |> order_by(asc: :name)
    |> preload(:category)
    |> Repo.all()
  end

  def list_editable_products_for_region(region) do
    edited_products = list_edited_regional_products_for_region(region)
    unedited_products = list_unedited_products_for_region(region)


    {:ok, edited_products: edited_products, unedited_products: unedited_products}
  end

  def list_edited_regional_products_for_region(region_slug) do
    query =
      (from rp in RegionalProduct,
            join: r in assoc(rp, :region),
            where: r.slug == ^region_slug,
            preload: [
              region: r,
            ],
            preload: [:type])
    Repo.all(query)
  end

  def list_unedited_products_for_region(region_slug) do
    edited =
      (from p in Product,
            where: fragment(
              """
              NOT EXISTS(
              SELECT NULL
              FROM regionalproducts AS rp
              INNER JOIN
                products ON products.id = rp.type_id
              INNER JOIN
                regions ON regions.id = rp.region_id
              WHERE rp.type_id = ? AND regions.slug =  ?
              )
              """,
              p.id,
              ^"#{region_slug}"

            ))
    Repo.all(edited)
  end

  def list_edited_products_for_region(region_slug) do
    query =
      (from p in Product,
            join: rp in assoc(p, :regional_products),
            join: r in assoc(rp, :region),
            where: r.slug == ^region_slug,
            preload: [
              regional_products: rp,
              regional_products: {
                rp,
                region: r
              }
            ])
    Repo.all(query)
  end

  @doc """
  Gets a single product.

  Raises `Ecto.NoResultsError` if the Product does not exist.

  ## Examples

      iex> get_product!(123)
      %Product{}

      iex> get_product!(456)
      ** (Ecto.NoResultsError)

  """
  def get_product!(slug) do
    query =
      from p in Product,
           where: p.slug == ^slug,
           preload: [:category]
    Repo.one(query)
  end

  def get_product_by_id!(id) do
    Repo.get!(Product, id)
  end

  @doc """
  Creates a product.

  ## Examples

      iex> create_product(%{field: value})
      {:ok, %Product{}}

      iex> create_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_product(attrs \\ %{}) do
    %Product{}
    |> Repo.preload(:category)
    |> Product.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a product.

  ## Examples

      iex> update_product(product, %{field: new_value})
      {:ok, %Product{}}

      iex> update_product(product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_product(%Product{} = product, attrs) do
    product
    |> Repo.preload(:category)
    |> Product.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Product.

  ## Examples

      iex> delete_product(product)
      {:ok, %Product{}}

      iex> delete_product(product)
      {:error, %Ecto.Changeset{}}

  """
  def delete_product(%Product{} = product) do
    Repo.delete(product)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking product changes.

  ## Examples

      iex> change_product(product)
      %Ecto.Changeset{source: %Product{}}

  """
  def change_product(%Product{} = product) do
    Product.changeset(product, %{})
  end

  @doc """
  Gets a single regional_product.

  Raises `Ecto.NoResultsError` if the Regional product does not exist.

  ## Examples

      iex> get_regional_product!(123)
      %RegionalProduct{}

      iex> get_regional_product!(456)
      ** (Ecto.NoResultsError)

  """
  def get_regional_product!(product_id, region_id) do

    query =
      from(
        rp in RegionalProduct,
        join: r in assoc(rp, :region),
        join: p in assoc(rp, :type),
        where: p.slug == ^product_id and r.slug == ^region_id,
        preload: [
          :region,
          :type,
          type: [:category]
        ]
      )

    Repo.one(query)
  end

  def get_products_for_region(region_id) do
    query =
      from(
        p in RegionalProduct,
        join: r in assoc(p, :region),
        where: r.slug == ^region_id,
        preload: [
          :type,
          type: [:category]
        ]
      )
    Repo.all(query)
  end

  @doc """
  Creates a regional_product.

  ## Examples

      iex> create_regional_product(%{field: value})
      {:ok, %RegionalProduct{}}

      iex> create_regional_product(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_regional_product(attrs \\ %{}) do
    %RegionalProduct{}
    |> RegionalProduct.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a regional_product.

  ## Examples

      iex> update_regional_product(regional_product, %{field: new_value})
      {:ok, %RegionalProduct{}}

      iex> update_regional_product(regional_product, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_regional_product(%RegionalProduct{} = regional_product, attrs) do
    regional_product
    |> RegionalProduct.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a RegionalProduct.

  ## Examples

      iex> delete_regional_product(regional_product)
      {:ok, %RegionalProduct{}}

      iex> delete_regional_product(regional_product)
      {:error, %Ecto.Changeset{}}

  """
  def delete_regional_product(%RegionalProduct{} = regional_product) do
    Repo.delete(regional_product)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking regional_product changes.

  ## Examples

      iex> change_regional_product(regional_product)
      %Ecto.Changeset{source: %RegionalProduct{}}

  """
  def change_regional_product(%RegionalProduct{} = regional_product) do
    RegionalProduct.changeset(regional_product, %{})
  end
  #
  #  alias SeasonalCalendar.Products.MonthData
  #
  #  @doc """
  #  Returns the list of monthdata.
  #
  #  ## Examples
  #
  #      iex> list_monthdata()
  #      [%MonthData{}, ...]
  #
  #  """
  #  def list_monthdata do
  #    Repo.all(MonthData)
  #  end
  #
  #  @doc """
  #  Gets a single month_data.
  #
  #  Raises `Ecto.NoResultsError` if the Month data does not exist.
  #
  #  ## Examples
  #
  #      iex> get_month_data!(123)
  #      %MonthData{}
  #
  #      iex> get_month_data!(456)
  #      ** (Ecto.NoResultsError)
  #
  #  """
  #  def get_month_data!(id), do: Repo.get!(MonthData, id)
  #
  #  @doc """
  #  Creates a month_data.
  #
  #  ## Examples
  #
  #      iex> create_month_data(%{field: value})
  #      {:ok, %MonthData{}}
  #
  #      iex> create_month_data(%{field: bad_value})
  #      {:error, %Ecto.Changeset{}}
  #
  #  """
  #  def create_month_data(attrs \\ %{}) do
  #    %MonthData{}
  #    |> MonthData.changeset(attrs)
  #    |> Repo.insert()
  #  end
  #
  #  @doc """
  #  Updates a month_data.
  #
  #  ## Examples
  #
  #      iex> update_month_data(month_data, %{field: new_value})
  #      {:ok, %MonthData{}}
  #
  #      iex> update_month_data(month_data, %{field: bad_value})
  #      {:error, %Ecto.Changeset{}}
  #
  #  """
  #  def update_month_data(%MonthData{} = month_data, attrs) do
  #    month_data
  #    |> MonthData.changeset(attrs)
  #    |> Repo.update()
  #  end
  #
  #  @doc """
  #  Deletes a MonthData.
  #
  #  ## Examples
  #
  #      iex> delete_month_data(month_data)
  #      {:ok, %MonthData{}}
  #
  #      iex> delete_month_data(month_data)
  #      {:error, %Ecto.Changeset{}}
  #
  #  """
  #  def delete_month_data(%MonthData{} = month_data) do
  #    Repo.delete(month_data)
  #  end
  #
  #  @doc """
  #  Returns an `%Ecto.Changeset{}` for tracking month_data changes.
  #
  #  ## Examples
  #
  #      iex> change_month_data(month_data)
  #      %Ecto.Changeset{source: %MonthData{}}
  #
  #  """
  #  def change_month_data(%MonthData{} = month_data) do
  #    MonthData.changeset(month_data, %{})
  #  end
end
