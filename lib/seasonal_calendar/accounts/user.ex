defmodule SeasonalCalendar.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  use Pow.Ecto.Schema
  use Pow.Extension.Ecto.Schema, extensions: [PowResetPassword, PowInvitation]

  schema "users" do
    field :name, :string
    field :role, :string, default: "user"
    pow_user_fields()
    many_to_many :regions, SeasonalCalendar.Regions.Region, join_through: "region_editors", on_delete: :delete_all, on_replace: :delete

    timestamps()
  end

  def changeset(user, attrs) do
    user
    |> pow_changeset(attrs)
    |> pow_extension_changeset(attrs)
    |> cast(attrs, [:name])
    |> validate_length(:name, min: 1, max: 20)
    |> unique_constraint(:name)
    |> changeset_role(attrs)
  end

  defp changeset_role(user, attrs) do
    user
    |> Ecto.Changeset.cast(attrs, [:role])
    |> Ecto.Changeset.validate_inclusion(:role, ~w(user admin))
  end
end



