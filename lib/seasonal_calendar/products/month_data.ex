defmodule SeasonalCalendar.Products.MonthData do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, except: [:id]}

  embedded_schema do
    field :april, :integer
    field :august, :integer
    field :december, :integer
    field :february, :integer
    field :january, :integer
    field :july, :integer
    field :june, :integer
    field :march, :integer
    field :may, :integer
    field :november, :integer
    field :october, :integer
    field :september, :integer

    timestamps()
  end

  @doc false
  def changeset(month_data, attrs) do
    month_data
    |> cast(attrs, [:january, :february, :march, :april, :may, :june, :july, :august, :september, :october, :november, :december])
    |> validate_required([:january, :february, :march, :april, :may, :june, :july, :august, :september, :october, :november, :december])
    |> validate_inclusion(:january, 0..3)
    |> validate_inclusion(:february, 0..3)
    |> validate_inclusion(:march, 0..3)
    |> validate_inclusion(:april, 0..3)
    |> validate_inclusion(:may, 0..3)
    |> validate_inclusion(:june, 0..3)
    |> validate_inclusion(:july, 0..3)
    |> validate_inclusion(:august, 0..3)
    |> validate_inclusion(:september, 0..3)
    |> validate_inclusion(:october, 0..3)
    |> validate_inclusion(:november, 0..3)
    |> validate_inclusion(:december, 0..3)
  end
end
