defmodule SeasonalCalendar.Products.RegionalProduct do
  use Ecto.Schema
  import Ecto.Changeset

  schema "regionalproducts" do
    field :name, :string
    field :slug, :string
    belongs_to :type, SeasonalCalendar.Products.Product
    belongs_to :region, SeasonalCalendar.Regions.Region
    embeds_one :availabilities, SeasonalCalendar.Products.MonthData

    timestamps()
  end

  @doc false
  def changeset(regional_product, attrs) do
    regional_product
    |> cast(attrs, [:name, :type_id, :region_id])
    |> validate_required([:name, :type_id, :region_id])
    |> cast_embed(:availabilities)
    |> assoc_constraint(:type)
    |> assoc_constraint(:region)
    |> slugify_name()
  end

  defp slugify_name(changeset) do
    case fetch_change(changeset, :name) do
      {:ok, new_name} -> put_change(changeset, :slug, slugify(new_name))
      :error -> changeset
    end
  end

  defp slugify(str) do
    str
    |> String.downcase()
    |> String.replace(~r/[^\w-]+/u, "-")
    |> String.replace("ü", "ue")
    |> String.replace("ä", "ae")
    |> String.replace("ö", "oe")
  end
end

defimpl Phoenix.Param, for: SeasonalCalendar.Products.RegionalProduct do
  def to_param(%{slug: slug}) do
    "#{slug}"
  end
end

