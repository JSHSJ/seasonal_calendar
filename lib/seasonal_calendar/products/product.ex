defmodule SeasonalCalendar.Products.Product do
  use Ecto.Schema
  import Ecto.Changeset

  schema "products" do
    field :name, :string
    field :slug, :string
    belongs_to :category, SeasonalCalendar.Categories.Category
    has_many :regional_products, SeasonalCalendar.Products.RegionalProduct, foreign_key: :type_id

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :category_id])
    |> validate_required([:name, :category_id])
    |> unique_constraint(:name)
    |> assoc_constraint(:category)
    |> slugify_name()
  end




  defp slugify_name(changeset) do
    case fetch_change(changeset, :name) do
      {:ok, new_name} -> put_change(changeset, :slug, slugify(new_name))
      :error -> changeset
    end
  end

  defp slugify(str) do
    str
    |> String.downcase()
    |> String.replace(~r/[^\w-]+/u, "-")
    |> String.replace("ü", "ue")
    |> String.replace("ä", "ae")
    |> String.replace("ö", "oe")
  end
end

defimpl Phoenix.Param, for: SeasonalCalendar.Products.Product do
  def to_param(%{slug: slug}) do
    "#{slug}"
  end
end

