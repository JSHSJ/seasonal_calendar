defmodule SeasonalCalendar.Regions.Region do
  use Ecto.Schema
  import Ecto.Changeset
  alias SeasonalCalendar.Accounts.User
  alias SeasonalCalendar.Repo

  schema "regions" do
    field :name, :string
    field :slug, :string
    has_many :products, SeasonalCalendar.Products.RegionalProduct
    many_to_many :editors, User, join_through: "region_editors", on_delete: :delete_all, on_replace: :delete

    timestamps()
  end

  @doc false

  def changeset(region, attrs) do
    region
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> slugify_name()
  end

  defp slugify_name(changeset) do
    case fetch_change(changeset, :name) do
      {:ok, new_name} -> put_change(changeset, :slug, slugify(new_name))
      :error -> changeset
    end
  end

  defp slugify(str) do
    str
    |> String.downcase()
    |> String.replace(~r/[^\w-]+/u, "-")
    |> String.replace("ü", "ue")
    |> String.replace("ä", "ae")
    |> String.replace("ö", "oe")
  end
end

defimpl Phoenix.Param, for: SeasonalCalendar.Regions.Region do
  def to_param(%{slug: slug}) do
    "#{slug}"
  end
end

