defmodule SeasonalCalendar.Repo do
  use Ecto.Repo,
    otp_app: :seasonal_calendar,
    adapter: Ecto.Adapters.Postgres
end
