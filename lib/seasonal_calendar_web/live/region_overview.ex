defmodule SeasonalCalendarWeb.RegionOverview do
  use Phoenix.LiveView

  alias SeasonalCalendarWeb.LayoutView
  alias SeasonalCalendar.Months

  def render(assigns) do
    ~L"""
    <div class="">
    <h2>Products</h2>

    <%= LayoutView.info_block info_visible: @info_visible, text: "An overview of all fruits and vegetables in " <> @region.name <>
    ". Use the filters or search to find what your looking for." %>
    <div class="action-container">
    <div class="select-buttons <%= @category %>">
      <button phx-click="filter_category" id="all" name="category" phx-value="all" class="select-button">All</button>
      <button phx-click="filter_category" id="vegetable" name="category" phx-value="vegetable" class="select-button">Vegetable</button>
      <button phx-click="filter_category" id="fruit" name="category" phx-value="fruit" class="select-button">Fruit</button>
    </div>
    <form phx-change="search" class="input-container"><input type="text" name="query" value="<%= @query %>" placeholder="Search..." /></form>
    </div>
    <div class="legend">
      <p class="legend-entry">
        <img src="/images/peak_season.png">Peak Season
      </p>
      <p class="legend-entry">
        <img src="/images/in_season.png">In Season
      </p>
      <p class="legend-entry">
        <img src="/images/stored.png">Stored
      </p>
      <p class="legend-entry">
        <img src="/images/not_available.png"></span>Not Available
      </p>
    </div>
    <div class="preview-container">
    <div class="product-preview">
      <div class="product-preview-text">

        </div>
          <div class="preview-grid labels">
            <div class="value-container one-one">
              <div class="label">1</div>
              <div class="label">2</div>
              <div class="label">3</div>
              <div class="label">4</div>
              <div class="label">5</div>
              <div class="label">6</div>
            </div>

            <div class="value-container one-two">
              <div class="label">7</div>
              <div class="label">8</div>
              <div class="label">9</div>
              <div class="label">10</div>
              <div class="label">11</div>
              <div class="label">12</div>
            </div>
          </div>
      </div>
      <div>
        <%= for product <- matching_products(assigns) do %>
        <a href="/regions/<%= @region.slug %>/products/<%= product.type.slug %>" class="product-preview">
        <div class="product-preview-text">
        <img src="/images/products/<%= product.type.slug %>.png" alt="<%= product.name %> Icon" onerror="this.src='/images/<%= product.type.category.slug %>.png'">
        <p><%= product.name %><span>
        <%= product.type.category.name %></span></p>
        </div>

        <div class="preview-grid">
        <div class="value-container one-one">
        <%= for month <- Months.list_months() |> Enum.slice(0,6)  do %>
          <div class="rating color-<%= month.slug %> rating-<%= get_month_rating(product, month) %> "></div>
        <% end %>
        </div>
        <div class="value-container one-two">
        <%= for month <- Months.list_months() |> Enum.slice(6, 11)  do %>
          <div class="rating color-<%= month.slug %> rating-<%= get_month_rating(product, month) %>"></div>
        <% end %>
        </div>
        </div>

        </a>
        <% end %>
      </div>
      </div>
    </div>
    """
  end

  def mount(session, socket) do
    {
      :ok,
      assign(
        socket,
        products: SeasonalCalendar.Products.get_products_for_region(session.region.slug),
        query: nil,
        category: "all",
        info_visible: false,
        region: session.region
      )
    }
  end

  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, assign(socket, query: query)}
  end

  def handle_event("filter_category", category, socket) do
    {:noreply, assign(socket, category: category)}
  end

  def handle_event("toggle_info", info_visible, socket) do
    case info_visible do
      "true" -> {:noreply, assign(socket, info_visible: false)}
      "false" -> {:noreply, assign(socket, info_visible: true)}
    end
  end

  defp get_text() do

  end

  defp matching_products(
         %{
           products: products,
           query: query,
           category: category
         }
       ) do
    products
    |> filter(query, category)
  end

  defp filter(products, query, category) do
    products
    |> filter_category(category)
    |> Enum.filter(&String.match?(&1.name, ~r/#{query}/i))
  end

  defp filter_category(products, category) do
    case category do
      "all" ->
        products
      _ ->
        products
        |> Enum.filter(&String.match?(&1.type.category.slug, ~r/#{category}/i))
    end
  end

  defp get_month_rating(product, month) do
    cond do
      Map.get(product.availabilities, String.to_atom(month.slug)) == 3 -> "peak-season value-peak-season"
      Map.get(product.availabilities, String.to_atom(month.slug)) == 2 -> "in-season value-in-season"
      Map.get(product.availabilities, String.to_atom(month.slug)) == 1 -> "stored value-stored"
      true -> "not-available value-not-available"
    end
  end
end
