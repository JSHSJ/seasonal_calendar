defmodule SeasonalCalendarWeb.Contributor.ProductOverview do
  use Phoenix.LiveView

  alias SeasonalCalendarWeb.Contributor.ProductView

  alias SeasonalCalendarWeb.LayoutView
  alias SeasonalCalendar.Products
  alias SeasonalCalendar.Regions


  def render(assigns) do
    ProductView.render("index.html", assigns)
  end

  def mount(session, socket) do
    assign(socket, current_user: session.current_user)
    {
      :ok,
      fetch(socket, session.current_user)
    }
  end

  defp fetch(socket, current_user) do
    editing_regions = []
    regions = Regions.get_editable_regions_for_user(current_user.id)

    editing_regions = Enum.map(
      regions,
      fn region ->
        case Products.list_editable_products_for_region(region.slug) do
          {:ok, products} ->
            edited_products = products[:edited_products]
            unedited_products = products[:unedited_products]
            region_data = %{
              :region => region,
              :edited_products => edited_products,
              :unedited_products => unedited_products
            }
            editing_regions = editing_regions ++ region_data
          _ -> {:error, nil}
        end
      end
    )

    assign(
      socket,
      editing_regions: editing_regions,
      query: nil,
      info_visible: false,
      region_filter: "all"
    )

  end

  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, assign(socket, query: query)}
  end


  def handle_event("toggle_info", info_visible, socket) do
    case info_visible do
      "true" -> {:noreply, assign(socket, info_visible: false)}
      "false" -> {:noreply, assign(socket, info_visible: true)}
    end
  end

  def handle_event("delete_product", query, socket) do
    r_p = query
          |> String.split(":")
    [region_id | r_p] = r_p
    [product_id | _] = r_p

    product = Products.get_regional_product!(product_id, region_id)
    {:ok, _product} = Products.delete_regional_product(product)

    {
      :noreply,
      socket
      |> put_flash(:info, "Product deleted successfully.")
      |> fetch(socket.assigns.current_user)
    }
  end

  def handle_event("filter_region", region_filter, socket) do
    {:noreply, assign(socket, region_filter: region_filter)}
  end
end
