defmodule SeasonalCalendarWeb.MonthDetailView do
  use Phoenix.LiveView

  alias SeasonalCalendarWeb.LayoutView

  def render(assigns) do
    ~L"""

    <div>
        <h2 class="month-headline">
        <span class="month-background color-<%= @month.slug %>">
        <svg xmlns="http://www.w3.org/2000/svg" style="isolation:isolate"
             viewBox="1252.074 747.71 36.919 175.777"><path
                    d=" M 1285.137 747.713 C 1276.971 746.943 1256.384 892.379 1252.074 923.487 C 1261.765 915.73 1273.219 906.577 1286.054 896.331 C 1288.43 842.647 1291.677 748.343 1285.137 747.713 L 1285.137 747.713 Z "/></svg>
        </span>
            <%= @month.name %>
        </h2>

        <%= LayoutView.info_block info_visible: @info_visible, text: "Overview of produce for #{@month.name}, sorted
          by availability." %>
        <div class="action-container">
            <div class="action-container">
                <div class="dropdown">
                    <h3 class="dropdown-headline"><%= category_text(@category) %><img src="/images/down.png"></h3>
                    <div class="select-buttons <%= @category %>">
                        <button phx-click="filter_category" id="all_categories" name="category" phx-value="all_categories"
                                class="select-button">All Categories
                        </button>
                        <button phx-click="filter_category" id="vegetable" name="category" phx-value="vegetable"
                                class="select-button">Vegetable
                        </button>
                        <button phx-click="filter_category" id="fruit" name="category" phx-value="fruit"
                                class="select-button">Fruit
                        </button>
                    </div>
                </div>
                <div class="dropdown">
                    <h3 class="dropdown-headline"><%= rating_text(@rating) %><img src="/images/down.png"></h3>
                    <div class="select-buttons <%= @rating %>">
                        <button phx-click="filter_rating" id="all_ratings" name="rating" phx-value="all_ratings"
                                class="select-button">All Ratings
                        </button>
                        <button phx-click="filter_rating" id="peak_season" name="rating" phx-value="peak_season"
                                class="select-button">Peak Season
                        </button>
                        <button phx-click="filter_rating" id="in_season" name="rating" phx-value="in_season"
                                class="select-button">In Season
                        </button>
                        <button phx-click="filter_rating" id="stored" name="rating" phx-value="stored"
                                class="select-button">Stored
                        </button>
                        <button phx-click="filter_rating" id="out_of_season" name="rating" phx-value="out_of_season"
                                class="select-button">Out of Season
                        </button>
                    </div>
                </div>
            </div>
            <form phx-change="search" class="input-container"><input type="text" name="query" value="<%= @query %>"
                                                                     placeholder="Search..."/></form>
        </div>

        <%= if @rating == "all_ratings" || @rating == "peak_season" do %>
        <div class="category-label">
            <div class="month-product--rating rating-peak-season color-<%= @month.slug %>">
            </div>
            <h3>Peak Season</h3>
        </div>
        <div class="month-productlist">
            <%= if length(matching_peak_season(assigns)) == 0 do %>
            <p>Nothing in peak season.</p>
            <% else %>
            <%= for product <- matching_peak_season(assigns) do %>
            <a href="/regions/<%= @region.slug %>/products/<%= product.type.slug %>" class="month-product">
                <img src="/images/products/<%= product.type.slug %>.png" alt="<%= product.name %> Icon"
                     onerror="this.src='/images/<%= product.type.category.slug %>.png'">
                <div class="month-product--text">
                    <p><%= product.name %></p>
                    <span><%= product.type.category.name %></span>
                </div>
            </a>
            <% end %>
            <% end %>
        </div>
        <% end %>

        <%= if @rating == "all_ratings" || @rating == "in_season" do %>
        <div class="category-label">
            <div class="month-product--rating rating-in-season color-<%= @month.slug %>">
            </div>
            <h3>In Season</h3>
        </div>
        <div class="month-productlist">
            <%= if length(matching_in_season(assigns)) == 0 do %>
            <p>Nothing in season.</p>
            <% else %>
            <%= for product <- matching_in_season(assigns) do %>
            <a href="/regions/<%= @region.slug %>/products/<%= product.type.slug %>" class="month-product">
                <img src="/images/products/<%= product.type.slug %>.png" alt="<%= product.name %> Icon"
                     onerror="this.src='/images/<%= product.type.category.slug %>.png'">

                <div class="month-product--text">
                    <p><%= product.name %></p>
                    <span><%= product.type.category.name %></span>
                </div>
            </a>
            <% end %>
            <% end %>
        </div>
        <% end %>

        <%= if @rating == "all_ratings" || @rating == "stored" do %>
        <div class="category-label">
            <div class="month-product--rating value-stored color-<%= @month.slug %>">
            </div>
            <h3>Available stored</h3>
        </div>
        <div class="month-productlist">
            <%= if length(matching_stored(assigns)) == 0 do %>
            <p>Nothing available stored.</p>
            <% else %>
            <%= for product <- matching_stored(assigns) do %>
            <a href="/regions/<%= @region.slug %>/products/<%= product.type.slug %>" class="month-product">
                <img src="/images/products/<%= product.type.slug %>.png" alt="<%= product.name %> Icon"
                     onerror="this.src='/images/<%= product.type.category.slug %>.png'">
                <div class="month-product--text">
                    <p><%= product.name %></p>
                    <span><%= product.type.category.name %></span>
                </div>
            </a>
            <% end %>
            <% end %>
        </div>
        <% end %>

        <%= if @rating == "all_ratings" || @rating == "out_of_season" do %>
        <div class="category-label">
            <div class="month-product--rating rating-not-available color-<%= @month.slug %>">
            </div>
            <h3>Out of season</h3>
        </div>
        <div class="month-productlist">
            <%= if length(matching_out_of_season(assigns)) == 0 do %>
            <% else %>
            <%= for product <- matching_out_of_season(assigns) do %>
            <a href="/regions/<%= @region.slug %>/products/<%= product.type.slug %>" class="month-product">
                <img src="/images/products/<%= product.type.slug %>.png" alt="<%= product.name %> Icon"
                     onerror="this.src='/images/<%= product.type.category.slug %>.png'">
                <div class="month-product--text">
                    <p><%= product.name %></p>
                    <span><%= product.type.category.name %></span>
                </div>
            </a>
            <% end %>
            <% end %>
        </div>
        <% end %>
    </div>
    """
  end

  def mount(session, socket) do
    {
      :ok,
      assign(
        socket,
        region: session.region,
        month: session.month,
        peak_season: session.peak_season,
        in_season: session.in_season,
        stored: session.stored,
        out_of_season: session.out_of_season,
        query: nil,
        category: "all_categories",
        rating: "all_ratings",
        info_visible: false
      )
    }
  end

  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, assign(socket, query: query)}
  end

  def handle_event("filter_category", category, socket) do
    {:noreply, assign(socket, category: category)}
  end

  def handle_event("filter_rating", rating, socket) do
    {:noreply, assign(socket, rating: rating)}
  end

  def handle_event("toggle_info", info_visible, socket) do
    case info_visible do
      "true" -> {:noreply, assign(socket, info_visible: false)}
      "false" -> {:noreply, assign(socket, info_visible: true)}
    end
  end

  defp matching_peak_season(
         %{
           peak_season: peak_season,
           query: query,
           category: category
         }
       ) do
    peak_season
    |> filter(query, category)
  end

  defp matching_in_season(
         %{
           in_season: in_season,
           query: query,
           category: category
         }
       ) do
    in_season
    |> filter(query, category)
  end

  defp matching_stored(
         %{
           stored: stored,
           query: query,
           category: category
         }
       ) do
    stored
    |> filter(query, category)
  end

  defp matching_out_of_season(
         %{
           out_of_season: out_of_season,
           query: query,
           category: category
         }
       ) do
    out_of_season
    |> filter(query, category)
  end

  defp filter(products, query, category) do
    products
    |> filter_category(category)
    |> Enum.filter(&String.match?(&1.name, ~r/#{query}/i))
  end

  defp filter_category(products, category) do
    case category do
      "all_categories" ->
        products
      _ ->
        products
        |> Enum.filter(
             fn p ->
               if String.match?(p.type.category.slug, ~r/#{category}/i) do
                 true
               end

             end
           )
    end
  end

  def category_text(category) do
    case category do
      "all_categories" -> "All Categories"
      "fruit" -> "Fruits"
      "vegetable" -> "Vegetables"
    end
  end

  def rating_text(rating) do
    case rating do
      "all_ratings" -> "All Ratings"
      "peak_season" -> "Peak Season"
      "in_season" -> "In Season"
      "stored" -> "Stored"
      "out_of_season" -> "Out of Season"
    end
  end
end
