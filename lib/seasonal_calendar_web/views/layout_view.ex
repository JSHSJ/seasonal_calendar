defmodule SeasonalCalendarWeb.LayoutView do
  use SeasonalCalendarWeb, :view

  def info_block(assigns) do
    render("info.html", assigns)
  end

  def is_region_path(path) do
    path
    |> String.match?(~r/(\/regions\/\w{1,})/i)
  end

  def current_region(path) do
    path
    |> String.split("/")
    |> Enum.at(2)
  end

  def generate_link_url(assigns) do
    path_segments = String.split(assigns.path, "/")
    [_ | path_segments] = path_segments
    [root | path_segments] = path_segments
    case length(path_segments) do
      0 -> "/regions/#{assigns.region}"
      _ -> case root do
             "regions" -> [_ | path_segments] = path_segments
                          "/#{root}/#{assigns.region}/#{
                            path_segments
                            |> Enum.join("/")
                          }"
             _ -> "/regions/#{assigns.region}"

           end
    end

  end
end
