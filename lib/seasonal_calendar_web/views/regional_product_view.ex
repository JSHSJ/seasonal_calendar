defmodule SeasonalCalendarWeb.RegionalProductView do
  use SeasonalCalendarWeb, :view

  def convert_availability_to_name(val) do
        cond do
          val == 3 -> "rating-peak-season value-peak-season"
          val == 2 -> "rating-in-season value-in-season"
          val == 1 -> "rating-stored value-stored"
          true -> "rating-not-available value-not-available"
        end
  end
end
