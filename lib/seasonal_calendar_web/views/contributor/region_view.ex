defmodule SeasonalCalendarWeb.Contributor.RegionView do
  use SeasonalCalendarWeb, :view

  def editor_select_options(users) do
    for user <- users, do: {user.name, user.id}
  end
end
