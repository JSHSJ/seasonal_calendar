defmodule SeasonalCalendarWeb.Contributor.ProductView do
  use SeasonalCalendarWeb, :view

  def matching_edited_products(products, query) do
    products
    |> Enum.filter(
         fn p ->
           if String.match?(p.name, ~r/#{query}/i) || String.match?(p.type.name, ~r/#{query}/i) do
             true
           end
         end
       )
  end

  def matching_unedited_products(products, query) do
    products
    |> Enum.filter(&String.match?(&1.name, ~r/#{query}/i))
  end


end
