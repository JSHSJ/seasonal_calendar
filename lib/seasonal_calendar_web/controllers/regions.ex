defmodule SeasonalCalendarWeb.Regions do
  import Plug.Conn
  alias SeasonalCalendar.Regions

  def init(opts), do: opts

  def call(conn, _opts) do
    all_regions = Regions.list_regions()
    assign(conn, :all_regions, all_regions)
  end
end
