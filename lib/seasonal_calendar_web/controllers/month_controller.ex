defmodule SeasonalCalendarWeb.MonthController do
  use SeasonalCalendarWeb, :controller

  alias SeasonalCalendar.Regions
  alias SeasonalCalendar.Months
  alias SeasonalCalendar.Products

  alias Phoenix.LiveView

  def index(conn, %{"region_id" => region_id }) do
      months = Months.list_months()
      render(conn, "index.html", region_id: region_id, months: months)
  end


  def show(conn, %{"region_id" => region_id, "id" => id}) do
    month = Months.get_month!(id)
    region = Regions.get_region!(region_id)
    products = Products.get_products_for_region(region_id)
    peak_season = products |> Enum.filter(fn p -> p.availabilities |> Map.get(String.to_atom(month.slug)) == 3 end)
    in_season = products |> Enum.filter(fn p -> p.availabilities |> Map.get(String.to_atom(month.slug)) == 2 end)
    stored = products |> Enum.filter(fn p -> p.availabilities |> Map.get(String.to_atom(month.slug)) == 1 end)
    out_of_season = products |> Enum.filter(fn p -> p.availabilities |> Map.get(String.to_atom(month.slug)) == 0 end)


    LiveView.Controller.live_render(conn, SeasonalCalendarWeb.MonthDetailView,
      session: %{
        month: month,
        region: region,
        peak_season: peak_season,
        in_season: in_season,
        stored: stored,
        out_of_season: out_of_season
      }
    )
  end

end
