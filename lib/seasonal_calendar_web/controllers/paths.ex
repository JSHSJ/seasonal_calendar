defmodule SeasonalCalendarWeb.Paths do
  import Plug.Conn

  def init(opts), do: opts

  def call(conn, _opts) do
    cur_path = Phoenix.Controller.current_path(conn)
    assign(conn, :current_path, cur_path)
  end
end
