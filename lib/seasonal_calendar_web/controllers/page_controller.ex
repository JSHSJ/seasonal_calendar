defmodule SeasonalCalendarWeb.PageController do
  use SeasonalCalendarWeb, :controller

  alias SeasonalCalendar.Regions

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
