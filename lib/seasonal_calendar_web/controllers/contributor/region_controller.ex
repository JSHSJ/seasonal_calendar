defmodule SeasonalCalendarWeb.Contributor.RegionController do
  use SeasonalCalendarWeb, :controller

  alias SeasonalCalendar.Regions
  alias SeasonalCalendar.Regions.Region
  alias SeasonalCalendar.Accounts


  plug :authenticate when action
  plug :load_editors when action in [:new, :create, :edit, :update]

  def index(conn, _params) do
    regions = Regions.list_regions()
    render(conn, "index.html", regions: regions)
  end

    def new(conn, _params) do
      changeset = Regions.change_region(%Region{editors: []})
      render(conn, "new.html", changeset: changeset)
    end

    def create(conn, %{"region" => region_params}) do
      case Regions.create_region(region_params) do
        {:ok, region} ->

          case Regions.update_region_editors(region, region_params) do
            {:ok, region} ->
              conn
              |> put_flash(:info, "Region created successfully.")
              |> redirect(to: Routes.contributor_region_path(conn, :index))

            {:error, %Ecto.Changeset{} = changeset} ->
              render(conn, "new.html", changeset: changeset)
          end


        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "new.html", changeset: changeset)
      end
    end

    def edit(conn, %{"id" => id}) do
      region = Regions.get_region!(id)
      changeset = Regions.change_region(region)
      render(conn, "edit.html", region: region, changeset: changeset)
    end

    def update(conn, %{"id" => id, "region" => region_params}) do
      region = Regions.get_region!(id)

      case Regions.update_region(region, region_params) do
        {:ok, region} ->
          case Regions.update_region_editors(region, region_params) do
            {:ok, region} ->
              conn
              |> put_flash(:info, "Region updated successfully.")
              |> redirect(to: Routes.contributor_region_path(conn, :index))

            {:error, %Ecto.Changeset{} = changeset} ->
              render(conn, "new.html", changeset: changeset)
          end

        {:error, %Ecto.Changeset{} = changeset} ->
          render(conn, "edit.html", region: region, changeset: changeset)
      end
    end

    def delete(conn, %{"id" => id}) do
      region = Regions.get_region!(id)
      {:ok, _region} = Regions.delete_region(region)

      conn
      |> put_flash(:info, "Region deleted successfully.")
      |> redirect(to: Routes.contributor_region_path(conn, :index))
    end

  defp load_editors(conn, _) do
    assign(conn, :users, Accounts.list_users())
  end

  defp authenticate(conn, _opts) do
    if conn.assigns.current_user do
      conn
    else
      conn
      |> put_flash(:error, "You must be logged in to access that page")
      |> redirect(to: Routes.page_path(conn, :index))
      |> halt()
    end
  end
end
