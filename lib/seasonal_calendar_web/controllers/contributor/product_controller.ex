defmodule SeasonalCalendarWeb.Contributor.ProductController do
  use SeasonalCalendarWeb, :controller

  alias SeasonalCalendar.Products
  alias SeasonalCalendar.Products.RegionalProduct
  alias SeasonalCalendar.Regions

  alias Phoenix.LiveView


  plug :authenticate

  def index(conn, _params) do
    current_user = conn.assigns.current_user
    LiveView.Controller.live_render(conn, SeasonalCalendarWeb.Contributor.ProductOverview,
      session: %{current_user: current_user}
    )
  end

  def new(conn, %{"product_id" => product_id, "region_id" => region_id}) do
    region = Regions.get_region!(region_id)
    type = Products.get_product!(product_id)
    changeset = Products.change_regional_product(%RegionalProduct{type_id: type.id, region_id: region.id})
    render(conn, "new.html", changeset: changeset, region_name: region.name, product_name: type.name)
  end

  def create(conn, %{"regional_product" => product_params}) do
    case Products.create_regional_product(product_params) do
      {:ok, product} ->
        conn
        |> put_flash(:info, "Product created successfully.")
        |> redirect(to: Routes.contributor_product_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        region = Regions.get_region_by_id!(changeset.changes.region_id)
        type = Products.get_product_by_id!(changeset.changes.type_id)
        render(conn, "new.html", changeset: changeset, region_name: region.name, product_name: type.name)
    end
  end

  def edit(conn, %{"product_id" => product_id, "region_id" => region_id}) do
    product = Products.get_regional_product!(product_id, region_id)
    region = Regions.get_region!(region_id)
    type = Products.get_product!(product_id)
    changeset = Products.change_regional_product(product)
    render(conn, "edit.html", product: product, changeset: changeset, region_id: region_id, product_id: product_id, region_name: region.name, product_name: type.name)
  end

  def update(conn, %{"product_id" => product_id, "region_id" => region_id, "regional_product" => product_params}) do
    product = Products.get_regional_product!(product_id, region_id)

    case Products.update_regional_product(product, product_params) do
      {:ok, product} ->
        conn
        |> put_flash(:info, "Product updated successfully.")
        |> redirect(to: Routes.contributor_product_path(conn, :index))

      {:error, %Ecto.Changeset{} = changeset} ->
        region = Regions.get_region!(region_id)
        type = Products.get_product!(product_id)
        render(conn, "edit.html", product: product, changeset: changeset, region_id: region.slug, product_id: type.slug, region_name: region.name, product_name: type.name)
    end
  end

  def delete(conn, %{"product_id" => product_id, "region_id" => region_id}) do
    product = Products.get_regional_product!(product_id, region_id)
    {:ok, _product} = Products.delete_regional_product(product)

    conn
    |> put_flash(:info, "Product deleted successfully.")
    |> redirect(to: Routes.contributor_product_path(conn, :index))
  end


  defp authenticate(conn, _opts) do
    if conn.assigns.current_user do
      conn
    else
      conn
      |> put_flash(:error, "You must be logged in to access that page")
      |> redirect(to: Routes.page_path(conn, :index))
      |> halt()
    end
  end
end
