defmodule SeasonalCalendarWeb.Contributor.UserController do
  use SeasonalCalendarWeb, :controller

  alias SeasonalCalendar.Accounts
  alias SeasonalCalendar.Accounts.User
  alias Ecto.Changeset

#  plug :authenticate when action in [:index]

#
#  def edit(conn, %{"id" => id}) do
#    user = Accounts.get_user!(id)
#    changeset = Accounts.change_user(user)
#    render(conn, "edit.html", user: user, changeset: changeset)
#  end

#  def register(conn, _params) do
#    changeset = Accounts.change_registration(%User{})
#    render(conn, "register.html", changeset: changeset)
#  end
#
#  def create(conn, %{"user" => user_params}) do
#    {code, params} = Map.pop(user_params, "invite_code")
#    case Accounts.get_token_by_string(String.trim(code)) do
#      %Token{} = token ->
#        %{is_admin: is_admin} = token
#        params = Map.put_new(params, "is_admin", is_admin)
#        Accounts.delete_token(token)
#        case Accounts.register_user(params) do
#                {:ok, user} ->
#                  conn
#                  |> SeasonalCalendarWeb.Auth.login(user)
#                  |> put_flash(:info, "Welcome aboard #{user.name}")
#                  |> redirect(to: Routes.contributor_contributor_path(conn, :index))
#
#                {:error, %Ecto.Changeset{} = changeset} ->
#                  render(conn, "register.html", changeset: changeset)
#              end
#
#      _ ->
#        {:error, %Ecto.Changeset{} = changeset} = Accounts.register_user(params)
#        changeset = Ecto.Changeset.add_error(changeset, :invite_code, "Invalid invite code")
#        render(conn, "register.html", changeset: changeset)
#    end
#  end
#
#
#  def edit(conn, %{"id" => id}) do
#    user = Accounts.get_user!(id)
#    changeset = Accounts.change_registration(user)
#    render(conn, "edit.html", user: user, changeset: changeset)
#  end
#
#
#  def delete(conn, %{"id" => id}) do
#    region = Accounts.get_user!(id)
#    {:ok, _region} = Accounts.delete_user(region)
#
#    conn
#    |> put_flash(:info, "User deleted successfully.")
#    |> redirect(to: Routes.contributor_user_path(conn, :index))
#  end
#
#  defp authenticate(conn, _opts) do
#    if conn.assigns.current_user do
#      conn
#    else
#      conn
#      |> put_flash(:error, "You must be logged in to access that page")
#      |> redirect(to: Routes.page_path(conn, :index))
#      |> halt()
#    end
#  end
#
#  # ADMIN
#
#  def index(conn, _params) do
#    users = Accounts.list_users()
#    render(conn, "index.html", users: users)
#  end
#
#  defp authenticate_admin(conn, _opts) do
#    if conn.assigns.current_user && conn.assigns.current_user.is_admin do
#      conn
#    else
#      conn
#      |> put_flash(:error, "You must be logged in to access that page")
#      |> redirect(to: Routes.page_path(conn, :index))
#      |> halt()
#    end
#  end


end
