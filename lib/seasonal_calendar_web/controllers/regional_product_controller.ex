defmodule SeasonalCalendarWeb.RegionalProductController do
  use SeasonalCalendarWeb, :controller

  alias SeasonalCalendar.Products
  alias SeasonalCalendar.Products.RegionalProduct
  alias SeasonalCalendar.Months


  def show(conn, %{"product_id" => product_id, "region_id" => region_id}) do
    regional_product = Products.get_regional_product!(product_id, region_id)
    months = Months.list_months() |> Enum.map(fn month -> month.slug end)
    render(conn, "show.html", regional_product: regional_product, months: months)
  end
end
