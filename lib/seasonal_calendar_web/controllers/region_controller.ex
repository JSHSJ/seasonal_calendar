defmodule SeasonalCalendarWeb.RegionController do
  use SeasonalCalendarWeb, :controller

  alias SeasonalCalendar.Regions
  alias SeasonalCalendar.Regions.Region

  alias Phoenix.LiveView

  def index(conn, _params) do
      render(conn, "index.html")
  end


  def show(conn, %{"id" => id}) do
    region = Regions.get_region!(id)

    LiveView.Controller.live_render(conn, SeasonalCalendarWeb.RegionOverview,
      session: %{region: region}
    )
  end

end
