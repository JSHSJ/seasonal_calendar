defmodule SeasonalCalendarWeb.Router do
  use SeasonalCalendarWeb, :router
  use Pow.Phoenix.Router
  use Pow.Extension.Phoenix.Router, otp_app: :seasonal_calendar

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug Phoenix.LiveView.Flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug SeasonalCalendarWeb.Regions
    plug SeasonalCalendarWeb.Paths
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :protected do
    plug Pow.Plug.RequireAuthenticated,
         error_handler: Pow.Phoenix.PlugErrorHandler
  end

  pipeline :admin_role do
    plug SeasonalCalendarWeb.EnsureRolePlug, :admin
  end

  scope "/contributor", PowInvitation.Phoenix, as: "pow_invitation" do
    pipe_through [:browser, :protected, :admin_role]

    resources "/invitations", InvitationController, only: [:new, :create, :show]
  end

  scope "/" do
    pipe_through :browser
    pow_routes()
    pow_extension_routes()
  end

  scope "/", SeasonalCalendarWeb do
    pipe_through :browser

    get "/", PageController, :index

    resources "/regions", RegionController, only: [:index, :show] do
      get "/products/:product_id", RegionalProductController, :show
      resources "/months", MonthController, only: [:index, :show]  do
      end
    end
  end

  scope "/contributor", SeasonalCalendarWeb.Contributor, as: :contributor do
    pipe_through [:browser, :protected]

    get "/", ContributorController, :index

    get "/products", ProductController, :index
    get "/products/:region_id/:product_id/edit", ProductController, :edit
    post "/products", ProductController, :create
    patch "/products/:region_id/:product_id", ProductController, :update
    put "/products/:region_id/:product_id", ProductController, :update
    delete "/products/:region_id/:product_id", ProductController, :delete
    get "/products/:region_id/:product_id/new", ProductController, :new
  end

  scope "/contributor", SeasonalCalendarWeb.Contributor, as: :contributor do
    pipe_through [:browser, :protected, :admin_role]

    resources "/categories", CategoryController, except: [:show]
    resources "/product-management", ProductManagementController
    resources "/regions", RegionController
  end
end
